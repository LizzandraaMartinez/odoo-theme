{
  'name':'Tutorial theme',
  'description': 'Tutorial de creación de Snippet.',
  'version':'1.13',
  'author':'Lizzie Martinez',

  'data': [
    'views/pages.xml',
    'views/snippets.xml',
  ],
  'category': 'Theme/Creative',
  'depends': ['website', 'website_theme_install'],
}